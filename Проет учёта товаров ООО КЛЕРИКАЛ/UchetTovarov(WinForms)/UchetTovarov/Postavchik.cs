﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UchetTovarov
{
    public partial class Postavchik : Form
    {
        public Postavchik()
        {
            InitializeComponent();
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Отображаетя текущая информация о поставщиках и возможность добавления новых.", "Справка", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
