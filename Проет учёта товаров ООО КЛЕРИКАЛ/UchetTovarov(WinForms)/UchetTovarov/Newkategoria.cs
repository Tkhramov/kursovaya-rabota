﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace UchetTovarov {
    public partial class Newkategoria : Form {
        public Newkategoria()
        {
            InitializeComponent();
        }


        private void button1_Click(object sender, EventArgs e) //Создание категории
        {
            if (textBox1.Text == "") MessageBox.Show("Пустое поле!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
            {
                using (SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-B80868I;Initial Catalog=UchetTovarov;Integrated Security=True"))
                {
                    con.Open();
                    string query = "INSERT INTO [KategoriyaTovaraVNomenklature] (Naimenovanie) VALUES ('" + textBox1.Text + "')";
                    SqlCommand com = new SqlCommand(query, con);
                    com.ExecuteNonQuery();
                    con.Close();
                }
                MessageBox.Show("Обновите список категорий (Значок обновить), для просмотра новых!", "Категория добавлена!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void infoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Добавление новой категории в номенклатуру товаров.", "Справка", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            textBox1.MaxLength = 100;
        }
    }
}
