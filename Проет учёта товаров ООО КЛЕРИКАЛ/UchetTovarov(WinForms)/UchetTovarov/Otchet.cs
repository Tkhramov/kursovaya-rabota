﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UchetTovarov
{
    public partial class Otchet : Form
    {
        public Otchet()
        {
            InitializeComponent();
        }


        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Отображается общая видоизменяемая информация о отчётах в организации.", "Справка", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

    }
}
