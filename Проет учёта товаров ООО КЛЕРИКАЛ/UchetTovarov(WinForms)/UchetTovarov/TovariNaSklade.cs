﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UchetTovarov
{
    public partial class TovariNaSklade : Form
    {
        public TovariNaSklade()
        {
            InitializeComponent();
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("В данном окне отображется информация о заказанных товарах.", "Справка", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
