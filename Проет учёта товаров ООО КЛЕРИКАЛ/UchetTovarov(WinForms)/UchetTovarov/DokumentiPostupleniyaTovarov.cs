﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UchetTovarov
{
    public partial class DokumentiPostupleniyaTovarov : Form
    {
        public DokumentiPostupleniyaTovarov()
        {
            InitializeComponent();
        }

        private void backToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Nomenklatura form = new Nomenklatura();
            this.Close();
            form.Visible = true;
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Отображаются все документы накладных по передвижению товара на предприятие.", "Справка", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
