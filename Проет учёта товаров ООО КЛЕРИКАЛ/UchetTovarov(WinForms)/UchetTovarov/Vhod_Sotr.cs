﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace UchetTovarov
{
    public partial class Vhod_Sotr : Form
    {
        public Vhod_Sotr()
        {
            InitializeComponent();
            FillCB();
        }


        void FillCB()
        {
            SqlConnection con = new SqlConnection(DBcon);

            try
            {
                con.Open();
                string query = "SELECT * FROM Sotrudniki";
                SqlCommand cmd = new SqlCommand(query, con);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    string name = sdr.GetString(1);
                    comboBox1.Items.Add(name);
                }
                con.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Clear()
        {
            textBox1.Clear();
            comboBox1.SelectedIndex = -1;
        }

        string DBcon = "Data Source=DESKTOP-B80868I;Initial Catalog=UchetTovarov;Integrated Security=True";
        private void pictureBox2_MouseEnter(object sender, EventArgs e)
        {
            pictureBox4.Visible = true;
        }

        private void pictureBox4_Click(object sender, EventArgs e) //Вход в программу
        {
            if (comboBox1.SelectedIndex == -1 || textBox1.Text == "")
            {
                if (comboBox1.SelectedIndex == -1 && textBox1.Text == "") MessageBox.Show("Поля не заполнены!", "Информация!", MessageBoxButtons.OK, MessageBoxIcon.Information);

                else if (comboBox1.SelectedIndex == -1)
                {
                    MessageBox.Show("Выберите Сотрудника!", "Информация!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                else if (textBox1.Text == "")
                {
                    MessageBox.Show("Введите пароль!", "Информация!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }

            else
            {
                using (SqlConnection con = new SqlConnection(DBcon))
                {
                    con.Open();
                    string SqlCommand = "SELECT FIO, Password FROM [Sotrudniki] WHERE FIO = '" + comboBox1.SelectedItem.ToString() + "' AND Password = '" + textBox1.Text + "'";
                    SqlDataAdapter sda = new SqlDataAdapter(SqlCommand, con);
                    DataTable dt = new DataTable();
                    sda.Fill(dt);
                    if (dt.Rows.Count == 1)
                    {
                        Nomenklatura form = new Nomenklatura();
                        this.Close();
                        form.Show();
                    }
                    else
                    {
                        MessageBox.Show("Неправильный пароль!", "Ошибка входа!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Clear();
                    }
                    con.Close();
                }
            }
        }

        private void SpravkaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Войти в систему как Сотрудник!", "Справка!", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            textBox1.MaxLength = 15;
            if (textBox1.Text != null)
            {
                textBox1.PasswordChar = '*';
                pictureBox5.Visible = true;
            }

            if (textBox1.Text == "")
            {
                pictureBox5.Visible = false;
                pictureBox6.Visible = false;
            }
        }

        private void pictureBox5_Click(object sender, EventArgs e) //Показать пароль
        {
            pictureBox5.Visible = false;
            textBox1.PasswordChar = default;
            pictureBox6.Visible = true; 
        }

        private void pictureBox6_Click(object sender, EventArgs e) //Скрыть пароль
        {
            pictureBox6.Visible = false;
            textBox1.PasswordChar = '*';
            pictureBox5.Visible = true;
        }

        private void pictureBox4_MouseEnter(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(pictureBox4, "Войти!");
        }

        private void pictureBox4_MouseLeave(object sender, EventArgs e)
        {
            pictureBox4.Visible = false;
        }

        private void Vhod_Sotr_FormClosing(object sender, FormClosingEventArgs e)
        {
            MainVhod form = new MainVhod();
            form.Visible = true;
        }
    }
}
