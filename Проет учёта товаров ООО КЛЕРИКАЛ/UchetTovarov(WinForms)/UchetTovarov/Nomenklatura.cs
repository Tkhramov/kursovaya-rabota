﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace UchetTovarov
{
    public partial class Nomenklatura : Form
    {
        public Nomenklatura()
        {
            InitializeComponent();
            FillTV();
        }

        string DBcon = "Data Source=DESKTOP-B80868I;Initial Catalog = UchetTovarov; Integrated Security = True";

        void FillTV() 
        {
            SqlConnection con = new SqlConnection(DBcon);
            try
            {
                con.Open();
                string query = "SELECT * FROM [KategoriyaTovaraVNomenklature]";
                SqlCommand cmd = new SqlCommand(query, con);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    TreeNode node = new TreeNode(sdr["Naimenovanie"].ToString());
                    treeView1.Nodes.Add(node);
                }
                con.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void backToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Действительно хотите сменить пользователя?", "Оповещение", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (result == DialogResult.Yes)
            {
                MainVhod form = new MainVhod();
                this.Close();
                form.Visible = true;
            }
            else return;
        }

        private void button3_Click(object sender, EventArgs e) 
        {
            NewTovar form = new NewTovar();
            form.ShowDialog();
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Данная программа предназначена для полного введения учёта товаров.", "О программе", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Newkategoria form = new Newkategoria();
            form.ShowDialog();
        }

        private void label2_MouseEnter(object sender, EventArgs e)
        {
            label2.Font = new Font(label2.Font, FontStyle.Italic | FontStyle.Underline);
        }

        private void label2_MouseLeave(object sender, EventArgs e)
        {
            label2.Font = new Font(label2.Font, FontStyle.Regular);
        }

        private void label3_MouseEnter(object sender, EventArgs e)
        {
            label3.Font = new Font(label3.Font, FontStyle.Italic | FontStyle.Underline);
        }

        private void label3_MouseLeave(object sender, EventArgs e)
        {
            label3.Font = new Font(label3.Font, FontStyle.Regular);
        }

        private void label4_MouseEnter(object sender, EventArgs e)
        {
            label4.Font = new Font(label4.Font, FontStyle.Italic | FontStyle.Underline);
        }

        private void label4_MouseLeave(object sender, EventArgs e)
        {
            label4.Font = new Font(label4.Font, FontStyle.Regular);
        }

        private void label5_MouseEnter(object sender, EventArgs e)
        {
            label5.Font = new Font(label5.Font, FontStyle.Italic | FontStyle.Underline);
        }

        private void label5_MouseLeave(object sender, EventArgs e)
        {
            label5.Font = new Font(label5.Font, FontStyle.Regular);
        }

        private void label6_MouseEnter(object sender, EventArgs e)
        {
            label6.Font = new Font(label6.Font, FontStyle.Italic | FontStyle.Underline);
        }

        private void label6_MouseLeave(object sender, EventArgs e)
        {
            label6.Font = new Font(label6.Font, FontStyle.Regular);
        }

        private void pictureBox9_MouseEnter(object sender, EventArgs e)
        {
            label2.Font = new Font(label2.Font, FontStyle.Italic | FontStyle.Underline);
        }

        private void pictureBox9_MouseLeave(object sender, EventArgs e)
        {
            label2.Font = new Font(label2.Font, FontStyle.Regular);
        }

        private void pictureBox10_MouseEnter(object sender, EventArgs e)
        {
            label3.Font = new Font(label3.Font, FontStyle.Italic | FontStyle.Underline);
        }

        private void pictureBox10_MouseLeave(object sender, EventArgs e)
        {
            label3.Font = new Font(label3.Font, FontStyle.Regular);
        }

        private void pictureBox11_MouseEnter(object sender, EventArgs e)
        {
            label4.Font = new Font(label4.Font, FontStyle.Italic | FontStyle.Underline);
        }

        private void pictureBox11_MouseLeave(object sender, EventArgs e)
        {
            label4.Font = new Font(label4.Font, FontStyle.Regular);
        }

        private void pictureBox13_MouseEnter(object sender, EventArgs e)
        {
            label6.Font = new Font(label6.Font, FontStyle.Italic | FontStyle.Underline);
        }

        private void pictureBox13_MouseLeave(object sender, EventArgs e)
        {
            label6.Font = new Font(label6.Font, FontStyle.Regular);
        }

        private void pictureBox12_MouseEnter(object sender, EventArgs e)
        {
            label5.Font = new Font(label5.Font, FontStyle.Italic | FontStyle.Underline);
        }

        private void pictureBox12_MouseLeave(object sender, EventArgs e)
        {
            label5.Font = new Font(label5.Font, FontStyle.Regular);
        }

        void allcbchf() 
        {
            radioButton1.Checked = false;
            radioButton2.Checked = false;
            radioButton3.Checked = false;
            radioButton4.Checked = false;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                allcbchf();
                groupBox3.Enabled = false;
            }
            
            if (checkBox1.Checked == false)
            {
                radioButton1.Checked = true;
                groupBox3.Enabled = true;
            }

        }

        private void pictureBox15_MouseEnter(object sender, EventArgs e)
        {
            RefKat.SetToolTip(pictureBox15, "Обновить список категорий");
        }

        private void pictureBox14_MouseEnter(object sender, EventArgs e)
        {
            DelKat.SetToolTip(pictureBox14, "Удалить категорию");
        }

        private void pictureBox2_MouseEnter(object sender, EventArgs e)
        {
            AddKat.SetToolTip(pictureBox2, "Добавить новую категорию");
        }

        private void pictureBox14_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Выберете категорию для удаления!", "Категория не выбрана!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void pictureBox9_Click(object sender, EventArgs e)
        {
            Predpriyatie form = new Predpriyatie();
            form.ShowDialog();
        }

        private void pictureBox10_Click(object sender, EventArgs e)
        {
            Sotrudniki form = new Sotrudniki();
            form.ShowDialog();
        }

        private void pictureBox11_Click(object sender, EventArgs e)
        {
            Postavchik form = new Postavchik();
            form.ShowDialog();
        }

        private void pictureBox12_Click(object sender, EventArgs e)
        {
            DokumentiPostupleniyaTovarov form = new DokumentiPostupleniyaTovarov();
            form.ShowDialog();
        }

        private void pictureBox13_Click(object sender, EventArgs e)
        {
            Otchet form = new Otchet();
            form.ShowDialog();
        }

        private void label2_Click(object sender, EventArgs e)
        {
            Predpriyatie form = new Predpriyatie();
            form.ShowDialog();
        }

        private void label3_Click(object sender, EventArgs e)
        {
            Sotrudniki form = new Sotrudniki();
            form.ShowDialog();
        }

        private void label4_Click(object sender, EventArgs e)
        {
            Postavchik form = new Postavchik();
            form.ShowDialog();
        }

        private void label5_Click(object sender, EventArgs e)
        {
            DokumentiPostupleniyaTovarov form = new DokumentiPostupleniyaTovarov();
            form.ShowDialog();
        }

        private void label6_Click(object sender, EventArgs e)
        {
            Otchet form = new Otchet();
            form.ShowDialog();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            ZakazTovarov form = new ZakazTovarov();
            form.ShowDialog();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            TovariNaSklade form = new TovariNaSklade();
            form.ShowDialog();
        }

        private void Nomenklatura_FormClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (MessageBox.Show("Действительно хотите завершить работу?", "Выход", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                Application.Exit();
            }
            else e.Cancel = true;
        }

        private void Nomenklatura_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "uchetTovarovDataSet.Nomenklatura_Tovarov". При необходимости она может быть перемещена или удалена.
            this.nomenklatura_TovarovTableAdapter.Fill(this.uchetTovarovDataSet.Nomenklatura_Tovarov);

        }
    }
}
