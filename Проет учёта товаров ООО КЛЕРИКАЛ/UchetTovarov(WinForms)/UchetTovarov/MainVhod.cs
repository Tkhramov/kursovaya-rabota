﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UchetTovarov
{
    public partial class MainVhod : Form
    {
        public static int vhod;
        public MainVhod()
        {
            InitializeComponent();
        }

        private void pictureBox1_MouseEnter(object sender, EventArgs e)
        {
            label2.Visible = true;
        }

        private void pictureBox2_MouseEnter(object sender, EventArgs e)
        {
            label3.Visible = true;
        }

        private void pictureBox1_MouseLeave(object sender, EventArgs e)
        {
            label2.Visible = false;
        }

        private void pictureBox2_MouseLeave(object sender, EventArgs e)
        {
            label3.Visible = false;
        }

          
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Vhod_Admin form = new Vhod_Admin();
            form.Show();
            this.Hide();
            vhod = 0;
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Vhod_Sotr form = new Vhod_Sotr();
            form.Show();
            this.Hide();
            vhod = 1;
        }

        private void SpravkaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Данное окно предаставляет выбор пользователя для входа в систему!", "Справка!", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void MainVhod_Load(object sender, EventArgs e)
        {

        }
    }
}
