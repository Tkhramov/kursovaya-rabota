﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace UchetTovarov {
    public partial class NewTovar : Form {
        public NewTovar()
        {
            InitializeComponent();
        }

        string DBcon = "Data Source=DESKTOP-B80868I;Initial Catalog=UchetTovarov;Integrated Security=True";

        void FillCB() 
        {
            comboBox1.Items.Clear();

            SqlConnection con = new SqlConnection(DBcon);

            try
            {
                con.Open();
                string query = "SELECT * FROM KategoriyaTovaraVNomenklature";
                SqlCommand cmd = new SqlCommand(query, con);
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    string name = sdr.GetString(1);
                    comboBox1.Items.Add(name);
                }
                con.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void backToolStripMenuItem_Click(object sender, EventArgs e) 
        {
            this.Close();
        }

        private void infoToolStripMenuItem_Click(object sender, EventArgs e) 
        {
            MessageBox.Show("Добавление нового товара в номенклатуру.","Справка", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Newkategoria form = new Newkategoria();
            form.ShowDialog();
        }

        private void comboBox1_Click(object sender, EventArgs e)
        {
            FillCB();
        }
    }
}
